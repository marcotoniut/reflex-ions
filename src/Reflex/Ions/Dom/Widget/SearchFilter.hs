{-# LANGUAGE ConstraintKinds, NoImplicitPrelude, NoMonomorphismRestriction, OverloadedStrings
           , RankNTypes, RecursiveDo, TypeFamilies #-}
{-# LANGUAGE ScopedTypeVariables #-}
module Reflex.Ions.Dom.Widget.SearchFilter where

import Control.Applicative (pure)
import Control.Lens ((.~), (^.), (&))
import Control.Monad.Fix (MonadFix)
import Data.Function (flip, ($), (.))
import Data.Functor ((<$), (<$>))
import Data.Map (Map)
import Data.Maybe (Maybe(Just))
import Data.Ord (Ord)
import Data.Text (Text)
import Data.Tuple (uncurry)
import Reflex (Reflex, MonadHold, PostBuild, Behavior, Event, attach, updated)
import Reflex.Dom.Core (
    DomBuilderSpace, DomBuilder, GhcjsDomSpace, InputElement, InputElementConfig
  , inputElement, _inputElement_value, _inputElementConfig_elementConfig, _elementConfig_initialAttributes
  , _inputElementConfig_setValue
  , button, def
  )

class FilterRow a where
  filterRow :: Text -> [a] -> [a]

type FilterWidgetConstraints t m =
  ( DomBuilder t m
  , MonadFix m
  , MonadHold t m
  , PostBuild t m
  , Reflex t
  )

filterWidget :: (FilterRow a, FilterWidgetConstraints t m) => Behavior t [a] -> m (Event t [a])
filterWidget = filterWidget' def

filterWidget' ::
     (FilterRow a, FilterWidgetConstraints t m)
  => InputElementConfig er t (DomBuilderSpace m)
  -> Behavior t [a]
  -> m (Event t [a])
filterWidget' conf b_xs = do
  rec
    i <- inputElement $ conf { _inputElementConfig_setValue = Just ("" <$ e_c) }
    e_c <- button ""
  pure (uncurry (flip filterRow) <$> attach b_xs (updated (_inputElement_value i)))


-- Traversable a => FilterRowM a
class FilterRowM a where
  filterRowM :: Text -> Map k a -> Map k a
  
filterWidgetM :: (FilterRowM a, FilterWidgetConstraints t m)  => Behavior t (Map k a) -> m (Event t (Map k a))
filterWidgetM = filterWidgetM' def

filterWidgetM' ::
     (FilterRowM a, FilterWidgetConstraints t m) 
  => InputElementConfig er t (DomBuilderSpace m)
  -> Behavior t (Map k a)
  -> m (Event t (Map k a))
filterWidgetM' conf b_xs = do
  rec
    i <- inputElement $ conf { _inputElementConfig_setValue = Just ("" <$ e_c) }
    e_c <- button ""
  pure (uncurry (flip filterRowM) <$> attach b_xs (updated (_inputElement_value i)))
