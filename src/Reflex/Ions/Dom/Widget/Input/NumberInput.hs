{-# LANGUAGE ConstraintKinds, GADTs, NoImplicitPrelude, OverloadedStrings, TemplateHaskell #-}
{-# LANGUAGE ScopedTypeVariables #-}
module Reflex.Ions.Dom.Widget.Input.NumberInput where

import Prelude (
    Bounded, Enum, Float, Integer, Num
  , concat, maxBound, minBound, succ
  )
import Control.Applicative (pure)
import Control.Lens ((^.), (.~))
import Control.Lens.TH (makeLenses)
import Control.Monad (liftM, mapM)
import Data.Bool (Bool)
import Data.Default (Default, def)
import Data.Function ((.), ($), (&))
import Data.Functor (fmap, (<$>))
import Data.Int (Int)
import Data.Map (Map)
import Data.Maybe (Maybe(Just), fromMaybe)
import Data.Monoid (mempty, (<>))
import Data.Text (Text, pack, unpack)
import Data.Word (Word, Word16)
import Reflex (Reflex, Event, Dynamic, constDyn, never)
import Reflex.Dom.Core (
    AttributeName, DomBuilder, DomBuilderSpace, GhcjsDomSpace, PostBuild
  , EventResult, EventName(Keypress, Keydown, Keyup), InputElement
  , domEvent, inputElement, value, initialAttributes
  , _inputElement_input, _inputElement_hasFocus
  , _inputElementConfig_initialValue, _inputElementConfig_setValue
  , (=:)
  )
import Text.Read (Read, readMaybe)
import Text.Show (Show, show)

import qualified Reflex as R
import qualified Prelude as P

-- sanitize :: String -> String
-- sanitize = map (\c -> if c == ',' then '.' else c)

type NumberInputConstraints a = (Bounded a, Default a, Enum a, Num a, Read a, Show a)

data NumberInput t a = (Reflex t, NumberInputConstraints a) => NumberInput
  { _numberInput_value :: Dynamic t (Maybe a)
  , _numberInput_input :: Event   t (Maybe a)
  , _numberInput_keypress  :: Event t Word
  , _numberInput_keydown   :: Event t Word
  , _numberInput_keyup     :: Event t Word
  , _numberInput_underflow :: Event t Text
  , _numberInput_overflow  :: Event t Text
  , _numberInput_hasFocus  :: Dynamic t Bool
  , _numberInput_builderElement :: InputElement EventResult GhcjsDomSpace t
  }

data NumberInputConfig t a = (Reflex t, NumberInputConstraints a) => NumberInputConfig
  { _numberInputConfig_initialValue :: Maybe a
  , _numberInputConfig_setValue :: Event t (Maybe a)
  , _numberInputConfig_step :: a
  , _numberInputConfig_max  :: a
  , _numberInputConfig_min  :: a
  , _numberInputConfig_initialAttributes :: Map AttributeName Text
  , _numberInputConfig_attributes :: Dynamic t (Map Text Text)
  }

concat <$> mapM makeLenses
  [ ''NumberInputConfig
  , ''NumberInput
  ]

numberInput ::
  ( DomBuilder t m
  , DomBuilderSpace m ~ GhcjsDomSpace
  , NumberInputConstraints a
  , PostBuild t m
  ) => NumberInputConfig t a
    -> m (NumberInput t a) 
numberInput c = do
  let iniVal   = fromMaybe "" . fmap tshow  $  _numberInputConfig_initialValue c
      e_setVal = fromMaybe "" . fmap tshow <$> _numberInputConfig_setValue     c
      iniAttr  = _numberInputConfig_initialAttributes c <>
        (  "type" =: "number"
        <> "lang" =: "en-150" -- TODO https://www.ctrl.blog/entry/html5-input-number-localization
        <> "step" =: (tshow $ _numberInputConfig_step c) -- c ^. numberInputConfig_step & tshow
        <> "min"  =: (tshow $ _numberInputConfig_min  c)
        <> "max"  =: (tshow $ _numberInputConfig_max  c)
        )
  i <- inputElement $ def
    { _inputElementConfig_initialValue = iniVal
    , _inputElementConfig_setValue = Just e_setVal
    } & initialAttributes .~ iniAttr
  pure $ NumberInput
    { _numberInput_value = readMaybe . unpack <$> value i -- Escape errors with Nothing
    , _numberInput_input = readMaybe . unpack <$> _inputElement_input i
    , _numberInput_keypress = domEvent Keypress i
    , _numberInput_keydown  = domEvent Keydown  i
    , _numberInput_keyup    = domEvent Keyup    i
    -- TODO
    , _numberInput_underflow = never 
    , _numberInput_overflow  = never
    , _numberInput_hasFocus  = _inputElement_hasFocus i
    , _numberInput_builderElement = i
    }
  where
    tshow :: Show a => a -> Text
    tshow = pack . show

instance (Reflex t, NumberInputConstraints a) => Default (NumberInputConfig t a) where
  {-# INLINABLE def #-}
  def = NumberInputConfig
    { _numberInputConfig_initialValue = def
    , _numberInputConfig_setValue     = never
    , _numberInputConfig_step = succ def
    , _numberInputConfig_min  = minBound
    , _numberInputConfig_max  = maxBound
    , _numberInputConfig_initialAttributes = mempty
    , _numberInputConfig_attributes = constDyn mempty
    }

-- _numberInput_element :: NumberInput t -> HTMLInputElement
-- _numberInput_element = _inputElement_raw . _numberInput_builderElement
