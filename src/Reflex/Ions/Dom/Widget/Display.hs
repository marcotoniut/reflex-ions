{-# LANGUAGE NoImplicitPrelude, OverloadedStrings, RankNTypes #-}
{-# LANGUAGE ScopedTypeVariables #-}
module Reflex.Ions.Dom.Widget.Display where

import Control.Applicative ((<*>))
import Control.Monad ((=<<), (>>=))
import Control.Monad.Fix (MonadFix)
import Data.Function ((.))
import Data.Functor (fmap, (<$>))
import Data.List (take, repeat, (++))
import Data.String (String)
import Data.Text (pack)
import Prelude (rem)
import Reflex (MonadHold, Dynamic, Event, PostBuild, holdDyn, fmapMaybe)
import Reflex.Dom.Core (count, constDyn, dynText)
import Reflex.Dom.Builder.Class (DomBuilder)

-- TODO Review constDyn
loadingText :: forall t m a b.
  ( DomBuilder t m
  , MonadFix m
  , MonadHold t m
  , PostBuild t m
  ) => Event t a
    -> m ()
loadingText e
  =   dynText . fmap pack . (constDyn ("Loading" ++) <*>)
  =<< fmap ((`take` repeat '.') . (`rem` 4)) <$> count e
