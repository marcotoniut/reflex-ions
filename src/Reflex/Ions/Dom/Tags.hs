{-# LANGUAGE NoImplicitPrelude, NoMonomorphismRestriction, OverloadedStrings, RankNTypes #-}
{-# OPTIONS_GHC -fno-warn-missing-signatures #-}
module Reflex.Ions.Dom.Tags where

import Control.Monad (mapM_)
import Data.Function ((.))
import Data.Map (singleton)
import Data.Monoid ((<>))
import Data.Text (Text, pack)
import Data.Word (Word8)
import Reflex.Dom.Core (
    DomBuilder, Element, EventResult, DomBuilderSpace
  , el, elAttr, elClass, elClass', divClass, text, blank
  , (=:)
  )
import Text.Show (Show, show)

hr_ :: DomBuilder t m => m ()
hr_ = el "hr" blank

showPack :: Show s => s -> Text
showPack = pack . show

div, main, table, thead, tfoot, tbody, colgroup, tr ::
  DomBuilder t m => m a -> m a
div      = el "div"
main     = el "main"
table    = el "table"
thead    = el "thead"
tfoot    = el "tfoot"
tbody    = el "tbody"
colgroup = el "colgroup"
tr       = el "tr"

div_, span_, header_, footer_, ul_, li_, table_, tr_, th_, td_ ::
  DomBuilder t m => Text -> m a -> m a
div_     = divClass
span_    = elClass "span"
header_  = elClass "header"
footer_  = elClass "footer"
ul_      = elClass "ul"
li_      = elClass "li"
table_   = elClass "table"
tr_      = elClass "tr"
th_      = elClass "th"
td_      = elClass "td"

li_'     = elClass' "li"

th_' :: DomBuilder t m => Text -> Word8 -> m a -> m a
th_' c s = elAttr "th" ("class" =: c <> "colspan" =: showPack s)

col :: DomBuilder t m => Word8 -> m ()
col s = elAttr "col" ("span" =: showPack s) blank

colgroup' :: DomBuilder t m => [Word8] -> m ()
colgroup' = colgroup . mapM_ col

bold, italic :: DomBuilder t m => Text -> m ()
bold   = el "b" . text
italic = el "i" . text

span_', h1_, h2_ :: DomBuilder t m => Text -> m ()
span_' = el "span" . text
h1_    = el "h1"   . text
h2_    = el "h2"   . text
h3_    = el "h3"   . text
h4_    = el "h4"   . text
