{-# LANGUAGE NoImplicitPrelude, RankNTypes #-}
{-# LANGUAGE ScopedTypeVariables #-}
module Reflex.Ions.Network where

import Control.Monad (sequence)
import Data.Maybe (Maybe, fromMaybe)
import Data.Function ((.))
import Data.Functor (fmap)
import Data.Traversable (Traversable)
import Reflex (Dynamic, Event, fmapMaybe)
import Reflex.Class (Reflex, MonadHold)
import Reflex.Adjustable.Class (Adjustable)
import Reflex.Network (networkView, networkHold)
import Reflex.NotReady.Class (NotReady)
import Reflex.PostBuild.Class (PostBuild)

import Helpers.Function ((<&.))

maybeNetworkHold ::
     (Reflex t, Adjustable t m, MonadHold t m)
  => (a -> m b)
  -> m b
  -> Event t (Maybe a)
  -> m (Dynamic t b)
maybeNetworkHold f os = networkHold os . fmapMaybe (fmap f)

maybeNetworkView ::
     (Reflex t, NotReady t m, Adjustable t m, PostBuild t m)
  => m b
  -> (a -> m b)
  -> Dynamic t (Maybe a)
  -> m (Event t b)
maybeNetworkView handleNothing handleJust = networkView . fmap (fromMaybe handleNothing . fmap handleJust)

simplerTrav ::
     (Reflex t, NotReady t m, Adjustable t m, PostBuild t m, Traversable tr)
  => Dynamic t (tr v)
  -> (v -> m a)
  -> m (Event t (tr a))
simplerTrav = networkView <&. fmap . fmap sequence . fmap
