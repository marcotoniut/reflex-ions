{-# LANGUAGE NoImplicitPrelude, ViewPatterns #-}
module Helpers.Either.Combinators (maybeToLeft, maybeToRight) where

import Data.Maybe (Maybe, maybe)
import Data.Either (Either(Left, Right))

maybeToLeft :: b -> Maybe a -> Either a b
maybeToLeft (Right -> r) = maybe r Left

maybeToRight :: b -> Maybe a -> Either b a
maybeToRight (Left -> l) = maybe l Right
