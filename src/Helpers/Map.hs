{-# LANGUAGE NoImplicitPrelude #-}
module Helpers.Map (mergeWith) where

import Data.Map (Map, union, intersectionWith)
import Data.Ord (Ord)

mergeWith :: Ord k => (a -> b -> b) -> Map k a -> Map k b -> Map k b
mergeWith f x y = intersectionWith f x y `union` y
