(import ./deps/reflex-platform {}).project ({ pkgs, ... }:
let nixpkgs = import <nixpkgs> {};
in
{        
  packages = {
    reflex-ions = ./. ; 
  };

  shells = {
    ghc = ["reflex-ions"];
    ghc8_2 = ["reflex-ions"];
    ghcjs = ["reflex-ions"];
  };

  withHoogle = false;
})
